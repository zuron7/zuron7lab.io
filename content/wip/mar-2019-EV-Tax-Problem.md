---
title: "The Electric Vehicle Tax Problem"
date: 2018-09-29T11:36:33+08:00
draft: true
featuredImg: ""
tags: 
  - gasoline
  - petrol
  - ev
  - tesla
  - electric
  - car
---

What is going to cover the massive tax revenues that the current and all previous governments have relied upon to fund themselves if electric cars become mainstream.
Will the tax on electricity go up?
Will electric vehicles still remain an attractive choice with higher electricity prices? 
Is this the golden age to enjoy EV's?
Like how the internet was 10 years ago?
 