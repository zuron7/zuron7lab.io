---
title: "Travelling without Google"
date: 2019-03-29T11:36:33+05:30
draft: false
toc: true
featuredImg: ""
tags: 
  - travel
  - google
  - conglomerates
---

It has been about a year since I went full self-hosted with my data. 
The only thing that isn't is my e-mail. Hosted by Yandex as of now. The entire process took me
a month of weekends to setup and was I glad.

I was seeing reductions in unneccessary ads, no unexpected UI/UX changes. Old design with old apps. 

Then came last week. I had to travel to Taipei, Taiwan. Never been to a predominantly chinese speaking country
before. The lack of Google Maps hit me hard. Everyone expects you to have maps, and not having it made it hard. 

I tried using OpenStreetMaps, but they don't really seem to be wll updated in these countries. Even in India, 
where I live, OSM isn't great, but is almost on par with Maps due to the need for a mapping solution which 
doesn't charge as much as google does. 

Additionally, living in Bangalore, there are a bunch of people who have made it their mission to keep OSM
as updated as possible. 

This made me realise the importance of having a good open map, and I am going to be doubling down on my promotion
of OSM among enthusiasts and the general public for non-navigational use intially. 

In addition, we need a good application for Android for editing maps. A lot of them tend to be half featured. 
