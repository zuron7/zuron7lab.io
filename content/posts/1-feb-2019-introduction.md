+++
title = "#1 - Welcome! Namaskara! Namaste! Willkommen!"
description = "Long form thoughts."
type = ["posts","post"]
date = "2019-02-24"
[ author ]
  name = "Varun Rajamane"
+++

This blog will be to serve as a list of my ideas and projects. Hope you can enjoy. 
I will also be posting videos often on my Youtube Channel. Smaller thoughts are expressed
on twitter.

But eventually, I want to have some sort of mirroring onto my own site over here. Wondering how to
do that automatically.