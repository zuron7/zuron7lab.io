---
title: "#2 - Installing Radicale on Alpine Linux"
date: 2019-03-10T18:19:13+05:30
draft: false
toc: true
featuredImg: ""
tags: 
  - server
  - self hosting
  - alpine
  - linux
  - radicale
  - installation
---

The radicale documentation does cover the installation of itself really well,
but Alpine Linux being quite a different OS, we have to make sure to take
care of a few things. 

I'm running the following configuration.
- Alpine Linux v3.8
- Radicale v2.1.0 stable

So, let's get started. 

# Step 1: Python installation

		apk update
		apk add python3

and that's it. That should install python3 on your system. 

----

# Step 2: Installing Radicale

For this step, we just follow the instructions from the website and get it from
pip.

		python3 -m pip install --upgrade radicale

For more information on getting the latest version, check 
https://radicale.org/download/ Now, I'm guessing you're running this in a VM
since you're using Alpine Linux so there's no point really to running this
to check whether it runs under localhost. So let's get it setup under an IP
Address so we can access it remotely. 

----

# Step 3: Radicale Config File

Now, radicale allows us to set it up either by passing arguments in the
command line or through a config file. Now, if you're barbaric or using this
only for the one time, I wouldn't pass so many arguments.

If you wish to use a password, you will need htpasswd from apache-utils,
use this line to install it. 

		apk add apache2-utils

Now, I'm creating my users file in my root directory, I remember a few people
saying this is a bad practice, but I ain't got the time for users. So this
will have to do. 

		htpasswd -B -c /path/to/users user_name

this will create a new user, which we'll use for radicale. Why do we use
htpasswd? Well, it provides some management tools, and more importantly,
encryption. Since we've used encryption, we need to teach radicale to 
read this gibberish, so install these dependencies. 

		apk add build-base
		apk add python3-dev
		apk add libffi
		apk add libffi-dev
		python3 -m pip install --upgrade radicale[bcrypt]

Now, that we're setup let us create the config file.

		[auth]
		type = htpasswd
		htpasswd_filename = /path/to/users
		#encryption method used in the htpasswd file
		htpasswd_encryption = bcrypt
		#Average delay after failed login attempts in seconds
		delay = 1
		
		[server]
		hosts = 0.0.0.0:5232
		max_connections = 20
		#100 Megabyte
		max_content_length = 100000000
		#30 seconds
		timeout = 30
		
		[storage]
		filesystem_folder = /path/to/storage

The authentication can be done with a plaintext file as well, you can
refer the radicale tutorial on more information on how to do that, but I
wouldn't reccomend it. Now that we've created our config file, let us 
run radicale. I would reccomend backing up the config file for future use
and ease of setup.

# Step 4: Verify that Radicale is working

Run this command

		python3 -m radicale --config "/path/to/config/"
 
and use your browser to point to the address that you used. You should
see a login page for radicale. 
	If so, success, now let us set up the service to make sure it runs
forever.

# Step 5: Setup Radicale as a service

Since Alpine Linux does not ship with systemd, the method is different
as from a regular linux distro. We have to use OpenRC. I used this
blog to familiarise myself with the system

[https://www.cyberciti.biz/faq/how-to-enable-and-start-services-on-alpine-linux/][OpenRC Services Guide - Cyberciti]

This is the service script. Put this into the /etc/init.d
or whatever other folder you prefer.

		#!/sbin/openrc-run
		
		pidfile=/var/run/radicale.pid

		command=/usr/bin/radicale
		command_args="--config "/path/to/config""
		command_background=yes
		
		depend() {
		    use net
		    need localmount
		}

Then ensure that rights to run the program are given

		chmod +x /path/to/service/script

Now, add this script to the OpenRC Boot

		rc-update add /path/to/service/script default

Now, on a reboot of the system, the script should run by itself. 
You can also manually start/stop it by:

		rc-service /path/to/service/script start
		rc-service /path/to/service/script stop 

And you're done. Enjoy your self-hosted contacts and calendar server.
Email me at <zuron7@rajamane.me> if you spot any errors. 

The End results was a server using 20 Megs of RAM at idle and around 250 MB
of space. That's one lightweight mountain.

# TO DO
Some improvements that can be done that this tutorial does not cover as
of yet. 

- Using TLS/SSL encryption with LetsEncrypt
- Using the hook feature to write changes to a git repository
- backups
- user access rights

Lot of this stuff is covered on the official website, so look it up.

Thank you for reading! Keep Self-Hosting!
Check out the self hosted community on reddit 
[https://reddit.com/r/selfhosted][Self Hosted Sub Reddit]